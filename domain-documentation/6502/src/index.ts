import { Instruction } from "./Types/Instruction";
import * as instructions from  "./data/instructions.json"

export class Main{
    instructions: Instruction[] = (instructions as unknown) as Instruction[]
}

const main = new Main();