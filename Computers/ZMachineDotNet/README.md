# Z-Machine.NET

Z-Machine.NET is a Z-Machine emulator which aims for bit-correct emulation for all Z-Code version 5 programs. Older versions should, usually, be supported by default, though this isn't always the case. Future versions _may_ be supported later.

## Overview

Importantly, Z-Machine.NET is a black box. Give it text input and it will spit out some output, be it text, audio or whatever. The goal, in the end, is to make this a part of a larger project which will enable alternate (and arbitrary) methods of interaction with Z-programs. For example, a web site which consists of a form and an output text area could be driven by a server which feeds the text to Z-Machine.NET (or, you know, you could implement a front-end in Blazor!).

The end game is to build a voice-controlled, immersive experience that will breathe new life into some wonderful pieces of video game (and, indeed, computing) history.

## Working With Z-Machine.NET

Generally speaking, industry-standard nomenclature, data types and data representation. For example, bytes are represented either as hexadecimal strings starting with `0x` or binary strings starting with `0b`. Bytes are bytes are bytes.

There are a few places, though, where non-standard nomenclature is used.

1. Rather than using a ushort (defined by .NET to be exactly two 8-bit bytes), which accurately describes what the Z-machine spec describes as a `word`, I (or rather, @binarycow) decided to implement a "custom type" (It isn't a type, it's a struct, but shhhh) called a `ZWord`. This is purely to ensure that, any place we're using a `word` according to the spec, we're being explicit. There may be a need, for whatever reason, to use a ushort in the code, and I wanted to make sure we are being explicit about what we mean.
2. Opcodes are represented in all documentation and in display output as they appear in the Z-machine spec: `<kind>:$<hex opcode>`. For example, `VAR:$C` for `call_fd`, or `0OP:$2` for `print`. This is because not all opcodes fit into a standard unit. Short opcodes, for example, are technically a nibble, while long opcodes are 5 bits long.

## Test Data

I am using a freely-available version of Zork (a.k.a. Dungeon) that I found which loads perfectly well in other emulators. You can find it over on [The Interactive Fiction Archive](https://ifarchive.org/indexes/if-archiveXgamesXzcode.html). 

### Disclaimer 

I make no guarantee that the files downloaded from the above site are legal. I am not aware of any legal issues around downloading these Z-programs, not least because there are protections in place in US copyright law that allow for copying and distributing software for which there exists no commercial product to run. That said, _do not push MRs with Z-programs in them_. They will be immediately closed. As in, the pipeline for this project will fail to run. If this triggers three times, your MR will be closed and you'll be temporarily banned from contributing to this project.

## Reference Sources

As of the beginning of the project, the only reference source being used is "[The Z-machine, And How To Emulate It](http://mirrors.ibiblio.org/interactive-fiction/infocom/z-machine/zspec02/zmach06e.pdf)", by Marnix Klooster. This doc dates back to _1996_. I am purposefully not referencing existing emulators nor their documentation. That doesn't mean contributors are _banned_ from referencing other emulators, mind you. If Frotz or whatever does something fancy that you think is a good fit for the emulator, feel free to throw up a merge request!

Eventually, I intend to build a mkdocs doc that goes into detail about the emulator as a whole, including a page for reference sources.

In the mean time, I'm creating a [REFERENCES.md](./REFERENCES.md) as a sort of dumping ground for Z-machine related resources.

## FAQ

**Q**: Why C#/.NET/Windows?

*A*: I (b4ux1t3) am a .NET developer by trade, and it's what I know. C# strikes a nice balance between language semantics and low-level control. As the Z-Machine isn't exactly a high-performance console, the efficiency I would gain from using a proper compiled language like Rust or C++ wouldn't even be noticeable. Still, if I need the oomph for more performance-critical features (Sound, maybe?), .NET is no slouch these days.

As for Windows: Z-Machine.NET supports all platforms that .NET 6 supports. That includes Windows, Mac, and most Linux distributions. Heck, you can even build it for [BSD](https://github.com/jasonpugsley/installer/wiki/.NET-Core-3.1.103-for-FreeBSD).
