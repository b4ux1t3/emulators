﻿using System;
using ZMachineDotNet.Types;
// ReSharper disable InconsistentNaming

namespace ZMachineDotNet
{
    public readonly struct ZProgram
    {
        private const int HeaderSize = 64;
        
        public ZHeader Header { get; private init; }
        public byte[] Bytes { get; }
        
        public ZProgram(byte[] zCodeBytes)
        {
            Bytes = zCodeBytes;
            var headerBytes = new byte[HeaderSize];
            for (var i = 0; i < HeaderSize; i++)
            {
                headerBytes[i] = zCodeBytes[i];
            }
            try
            {
                Header = new ZHeader(headerBytes);
            }
            catch (InvalidHeaderLengthException e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        
    }
}
