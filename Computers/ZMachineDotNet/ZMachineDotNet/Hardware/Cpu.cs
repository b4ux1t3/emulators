﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZMachineDotNet.Types;
using ZMachineDotNet.Types.Opcode;

namespace ZMachineDotNet.Hardware;

public class Cpu
{
    private readonly Stack<Frame> CallStack;
    private readonly List<byte> SystemMemory;
    private readonly RngCard Rng;
    private readonly IoCard SystemIo;
    private readonly VideoCard Display;

    public Cpu(List<byte> systemMemory, Stack<Frame> callStack, IoCard systemIo, VideoCard display, RngCard rng)
    {
        SystemIo = systemIo;
        Display = display;
        Rng = rng;
        SystemMemory = systemMemory;
        CallStack = callStack;
    }

    /// <summary>
    /// Continuously calls <see cref="Execute"/> until the program ends.
    /// </summary>
    public void Run()
    {
        var run = true;
        while (run)
        {
            run = Execute();
        }
    }
    
    /// <summary>
    /// Executes the next frame on the stack.
    /// </summary>
    /// <returns>True if there are more instructions to run, false if the program has ended.</returns>
    private bool Execute()
    {
        var currentFrame = CallStack.Peek();
        var parsedInstruction = ParseInstruction(currentFrame.ProgramCounter);
        DebugExecution(currentFrame, parsedInstruction);
        var returnedProgramCounter = ExecuteInstruction(parsedInstruction);
        
        return false;
    }
    
    /// <summary>
    /// Performs some action based on the opcode provided.
    /// </summary>
    /// <param name="programCounter"></param>
    /// <returns>A parsed <see cref="ZInstruction"/>.</returns>
    private ZInstruction ParseInstruction(ZWord programCounter)
    {
        var instruction = SystemMemory[programCounter];
        var opCodeKind = ZInstruction.GetOpCodeKind(instruction);
        ZInstruction newInstruction;
        switch (opCodeKind)
        {
            case Kind.ZeroOp:
                newInstruction = new ZInstruction(instruction, Array.Empty<byte>(), opCodeKind);
                break;
            case Kind.OneOp:
                newInstruction = new ZInstruction(instruction, new []{SystemMemory[programCounter+1]}, opCodeKind);
                break;
            case Kind.TwoOp:
                newInstruction = new ZInstruction(instruction,
                    new[] {SystemMemory[programCounter + 1], SystemMemory[programCounter + 2]}, opCodeKind);
                break;
            case Kind.Var:
                var typeByte = SystemMemory[programCounter + 1];
                var numOperands = ZInstruction.GetVarOperands(typeByte);
                var operands = new byte[numOperands];
                for (var i = 0; i < operands.Length; i++)
                {
                    // programCounter + 2 + i because +1 is our typeByte,
                    // so we need to +2 to get to the byte after that,
                    // and then +i to get to the appropriate byte!
                    operands[i] = SystemMemory[programCounter + 2 + i];
                }
                newInstruction = new ZInstruction(instruction, operands, opCodeKind, typeByte);
                break;
            case Kind.Ext:
            default:
                throw new NotImplementedException($"We don't support {opCodeKind.ToString()} opcodes. (yet)");
                
        }
        return newInstruction;
    }
    
    /// <summary>
    /// Updates the program state, given a <see cref="ZInstruction"/>.
    /// </summary>
    /// <param name="instruction"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    private ZWord ExecuteInstruction(ZInstruction instruction)
    {
        
        throw new NotImplementedException("We can't, you know, execute instructions yet.");
    }

    private static void DebugExecution(Frame currentFrame, ZInstruction currentInstruction)
    {
        var instructionStringLines = currentInstruction.ToString().Split("\n");
        var frameStringLines = currentFrame.ToString().Split("\n");
        
        // Get the length of the longest line.
        var longestLineLength = frameStringLines.Select(line => line.Length).Prepend(0).Max();

        // Add spaces to the end of our shorter lines.
        var formattedFrameLines = new string[frameStringLines.Length];
        for (var i = 0; i < frameStringLines.Length; i++)
        {
            if (frameStringLines[i].Length < longestLineLength)
            {
                var sbInner = new StringBuilder();
                sbInner.Append(frameStringLines[i]);
                for (var j = 0; j < longestLineLength - frameStringLines[i].Length; j++)
                {
                    sbInner.Append(' ');
                }

                formattedFrameLines[i] = sbInner.ToString();
            }
            else
            {
                formattedFrameLines[i] = frameStringLines[i];
            }
        }

        
        for (var i = 0; i < instructionStringLines.Length; i++)
        {
            formattedFrameLines[i] = $"{formattedFrameLines[i]} | {instructionStringLines[i]}";
        }
        
        Console.WriteLine(string.Join('\n', formattedFrameLines));
    }

}