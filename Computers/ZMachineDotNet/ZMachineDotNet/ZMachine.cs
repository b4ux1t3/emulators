﻿using System;
using System.Collections.Generic;
using ZMachineDotNet.Hardware;
using ZMachineDotNet.Types;

namespace ZMachineDotNet;

/// <summary>
/// Can be thought of as the Z-Machine as a whole, including the "bootloader".
/// </summary>
/// <remarks>
/// Initializes all of the "hardware", loads the Z-program into memory, and then passes all of the initialized pieces to
/// the CPU for execution.
/// </remarks>

public class ZMachine
{
    private readonly Cpu _cpu;
    private readonly ZProgram _loadedProgram;
    private readonly Stack<Frame> _callStack;

    /// <summary>Create a new Z-machine.</summary>
    /// <remarks>
    /// All of the "hardware" is created here, and then passed to the CPU. 
    /// </remarks>
    /// <param name="loadedProgramBytes">A <code>byte[]</code> containing the bytes from a Z-code file.</param>
    /// <param name="memorySize">Optionally limit the size of the available memory. Without</param>
    public ZMachine(byte[] loadedProgramBytes, int? memorySize = null)
    {
        List<byte> systemMemory;
        _loadedProgram = new ZProgram(loadedProgramBytes);
        if (memorySize is not null)
        {
            systemMemory = loadedProgramBytes.Length > memorySize
                ? new List<byte>((int) memorySize)
                // It is recommended that a Z-machine behave as though it has enough memory to support any arbitrary
                // program, even if it's longer than the "recommended" memory size. 
                : new List<byte>(loadedProgramBytes.Length);  
        }
        else
        {
            // UNLIMITED MEMORY!!!
            // There is, technically, no limit to the size of a Z-machine's memory.
            // This represents a "technically pure" Z-machine.
            systemMemory = new List<byte>(); 
        }

        systemMemory.AddRange(loadedProgramBytes);
        _callStack = new Stack<Frame>();
        var rng = new RngCard();
        var systemIo = new IoCard();
        var display = new VideoCard();
        _cpu = new Cpu(systemMemory, _callStack, systemIo, display, rng);
    }
    
    /// <summary>
    /// 
    /// </summary>
    public void Initialize()
    {
        _callStack.Push(new Frame(_loadedProgram.Header.FirstInstructionAddress, Array.Empty<ZWord>(), CallMethod.Default));
        _cpu.Run();
    }

}