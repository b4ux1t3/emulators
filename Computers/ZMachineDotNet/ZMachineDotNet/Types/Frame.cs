﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZMachineDotNet.Types;

public enum CallMethod
{
    Default,
    Function,
    Procedure,
    Interrupt,
}
public record Frame
{
    // this can technically just be an int or long, as it is just supposed to be a natural number.
    // It is usually limited for practical reasons
    public ZWord ProgramCounter { get; }
    private readonly Stack<ZWord> _routineStack;
    private readonly ZWord[] _localVariables;
    private readonly CallMethod _callMethod;
    private readonly uint _argCount;
    private readonly ZWord _framePointer;

    public Frame(ZWord programCounter, ZWord[] arguments, CallMethod callMethod, ZWord? framePointer = null)
    {
        ProgramCounter = programCounter;
        _localVariables = new ZWord[15];
        _callMethod = callMethod;
        _routineStack = new Stack<ZWord>();
        
        if (framePointer is not null) _framePointer = (ZWord) framePointer;

        _argCount = 0;
        for (var i = 0; i < arguments.Length; i++)
        {
            _localVariables[i] = arguments[i];
            _argCount++;
        }
    }

    public override string ToString()
    {
        // our "table" should always be this wide.
        const string tableSeparator = "---------------------------------";
        var sb = new StringBuilder();
        sb.Append("Current Frame\n");
        sb.Append($"{tableSeparator}\n");
        
        // We'll use these throughout the method.
        var headerString = "PC:";
        var tempString = $"0x{ProgramCounter:x}";

        // Add the program counter
        sb.Append(PadSpaces(headerString, tempString, tableSeparator.Length));
        
        
        // Add the call method
        headerString = "Method:";
        tempString = $"{_callMethod.ToString()}";
        
        sb.Append(PadSpaces(headerString, tempString, tableSeparator.Length));

        // Add ArgC
        headerString = "ArgC:";
        tempString = $"{_argCount}";
        
        sb.Append(PadSpaces(headerString, tempString, tableSeparator.Length));
        
        // Add Stack Size
        sb.Append(PadSpaces(headerString, tempString, tableSeparator.Length));
        
        // Now just add our local variables.
        sb.Append("Local Variables\n");
        
        sb.Append($"{tableSeparator}\n");
        for (var i = 0; i < _localVariables.Length / 3; i += 1)
        {
            sb.Append($"{i:x}: 0x{_localVariables[i]:X4} | {i + 5:x}: 0x{_localVariables[i + 5]:X4} | {i + 10:x}: 0x{_localVariables[i + 10]:X4}\n");
        }
        sb.Append($"{tableSeparator}\n");
        return sb.ToString();
    }

    private static string PadSpaces(string headerString, string bodyString, int lineLength)
    {
        var sb = new StringBuilder();
        sb.Append(headerString);
        for (var i = 0; i < lineLength - (headerString.Length + bodyString.Length); i++)
        {
            sb.Append(' ');
        }

        sb.Append($"{bodyString}\n");

        return sb.ToString();
    }
}
