﻿using System.Text;
using ZMachineDotNet.Types.Opcode;

namespace ZMachineDotNet.Types
{


    namespace Opcode
    {
        public enum Kind
        {
            ZeroOp,
            OneOp,
            TwoOp,
            Var,
            Ext,
        }
    }

    /// <summary>
    /// Model for an instruction, including:
    ///   * The KIND of instruction (0OP, 1OP, 2OP, VAR, EXT)
    ///   * The instruction itself
    ///   * The operand(s), if any
    /// </summary>
    public readonly struct ZInstruction
    {
        public readonly Kind Kind;
        public readonly byte Instruction;
        public readonly byte[] Operands;
        public readonly byte? TypeByte;
        public readonly byte Opcode;

        public ZInstruction(byte instruction, byte[] operands, Kind kind, byte? typeByte = null)
        {
            Instruction = instruction;
            Operands = operands;
            Kind = kind;
            TypeByte = typeByte;
            Opcode = ExtractOpCode(instruction);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("Current Instruction\n");
            sb.Append("-------------------\n");
            sb.Append($"Instruction: 0x{Instruction:x2} | Opcode: {Kind.ToString()}:${Opcode:x2}\n");
            sb.Append("Type Byte: ");
            if (TypeByte is not null)
            {
                var typeByteString = "";
                for (var i = 0; i < 8; i++)
                {
                    typeByteString += $"{(((TypeByte << i) & 0b10000000) == 0b10000000 ? 1 : 0)}";
                }

                sb.Append($"0b{typeByteString} (0x{TypeByte:x2})");
            }

            sb.Append('\n');
            if (Operands.Length <= 0) return sb.ToString();
            sb.Append("Operands\n");
            var index = 0;
            foreach (var op in Operands)
            {
                sb.Append($"{index++}: 0x{op:x2} | ");
            }

            return sb.ToString();
        }

        public static Kind GetOpCodeKind(byte instruction)
        {
            const byte shortOpFlag = 0b10000000;
            const byte shortOrVarOpMask = 0b11000000; // We'll name this appropriately so it isn't confusing later!
            if ((instruction & shortOrVarOpMask) == shortOpFlag)
            {
                const byte zeroOpFlag = 0b00110000;
                return (instruction & zeroOpFlag) == zeroOpFlag ? Kind.ZeroOp : Kind.OneOp;
            }

            const byte longOpFlag = 0b00000000;
            const byte longOpTypeMask = 0b00100000;
            // we'll re-use shortOp, because it matches the longOp mask.
            // This will look confusing, but it saves us a whole byte!
            if ((instruction & shortOpFlag) == longOpFlag) return Kind.TwoOp;
            // Aaaaand we'll need to check for VAR, which is the same as its mask.
            if ((instruction & shortOrVarOpMask) == shortOrVarOpMask)
            {
                // And, for our last re-use, we're going to use longOpTypeMask, because it matches
                // BOTH the varOpMask and the value we're looking for!
                return (instruction & longOpTypeMask) == longOpTypeMask ? Kind.Var : Kind.TwoOp;
            }
            // If it isn't any of these, it's probably an extension instruction.
            // If it isn't, that's okay, we'll ignore extension opcodes for now anyway.
            return Kind.Ext;
        }

        public static int GetVarOperands(byte typeByte)
        {
            var count = 0;
            for (var i = 0; i < 4; i++)
            {
                const byte maskBits = 0b11000000;
                count += ((typeByte << i * 2) & maskBits) == maskBits ? 0 : 1;
            }

            return count;
        }

        private static byte ExtractOpCode(byte instruction)
        {
            var kind = GetOpCodeKind(instruction);
            const byte shortMask = 0b00001111;
            const byte longMask = 0b00011111;

            if (kind is Kind.ZeroOp or Kind.OneOp)
            {
                return (byte) (instruction & shortMask);
            }

            return (byte) (instruction & longMask);
        }
    }
}