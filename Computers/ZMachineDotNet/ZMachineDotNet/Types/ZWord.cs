﻿using System;
using System.Collections.Generic;

namespace ZMachineDotNet.Types
{
    public readonly struct ZWord : IEquatable<ZWord>, IComparable<ZWord>, IComparable, IFormattable
    {
        public static readonly ZWordComparer Comparer = ZWordComparer.Instance;
        private ushort Value { get; }
        public ZWord(ushort value) => this.Value = value;
        public ZWord(byte value) => this.Value = value;


        public override string ToString()
        {
            return Value.ToString();
        }

        public string ToString(string? format, IFormatProvider? formatProvider)
        {
            return Value.ToString(format, formatProvider);
        }

        public int CompareTo(ZWord other) => this.Value.CompareTo(other.Value);
        public int CompareTo(object? obj) => obj switch
        {
            null => 1,
            ZWord other => CompareTo(other),
            _ => throw new ArgumentException($"Object must be of type {nameof(ZWord)}"),
        };
        public bool Equals(ZWord other) => this.Value == other.Value;
        public override bool Equals(object? obj) => obj is ZWord other && Equals(other);
        public override int GetHashCode() => this.Value.GetHashCode();
        public static explicit operator byte(ZWord value) => (byte)value.Value;
        public static implicit operator ushort(ZWord value) => value.Value;
        public static implicit operator uint(ZWord value) => value.Value;
        public static implicit operator ulong(ZWord value) => value.Value;
        public static explicit operator sbyte(ZWord value) => (sbyte)value.Value;
        public static explicit operator short(ZWord value) => (short)value.Value;
        public static implicit operator int(ZWord value) => value.Value;
        public static implicit operator long(ZWord value) => value.Value;
        public static implicit operator double(ZWord value) => value.Value;
        public static implicit operator float(ZWord value) => value.Value;
        public static implicit operator decimal(ZWord value) => value.Value;
        public static implicit operator ZWord(byte value) => new ZWord(value);
        public static implicit operator ZWord(ushort value) => new ZWord(value);
        public static explicit operator ZWord(uint value) => new ZWord((ushort)value);
        public static explicit operator ZWord(ulong value) => new ZWord((ushort)value);
        public static explicit operator ZWord(sbyte value) => new ZWord((ushort)value);
        public static explicit operator ZWord(short value) => new ZWord((ushort)value);
        public static explicit operator ZWord(int value) => new ZWord((ushort)value);
        public static explicit operator ZWord(long value) => new ZWord((ushort)value);
        public static explicit operator ZWord(decimal value) => new ZWord((ushort)value);
        public static explicit operator ZWord(double value) => new ZWord((ushort)value);
        public static explicit operator ZWord(float value) => new ZWord((ushort)value);
        
        public static ZWord operator +(ZWord a) => a;   
        public static ZWord operator ~(ZWord a) => new ZWord((ushort)(~a.Value));
        public static bool operator ==(ZWord left, ZWord right) => left.Value == right.Value;
        public static bool operator !=(ZWord left, ZWord right) => left.Value != right.Value;
        public static bool operator <(ZWord left, ZWord right) => left.Value < right.Value;
        public static bool operator >(ZWord left, ZWord right) => left.Value > right.Value;
        public static bool operator <=(ZWord left, ZWord right) => left.Value <= right.Value;
        public static bool operator >=(ZWord left, ZWord right) => left.Value >= right.Value;
        
        public static ZWord operator +(ZWord a, ZWord b) => new ZWord((ushort)(a.Value + b.Value));
        public static ZWord operator -(ZWord a, ZWord b) => new ZWord((ushort)(a.Value - b.Value));
        public static ZWord operator *(ZWord a, ZWord b) => new ZWord((ushort)(a.Value * b.Value));
        public static ZWord operator /(ZWord a, ZWord b) => new ZWord((ushort)(a.Value / b.Value));
        public static ZWord operator %(ZWord a, ZWord b) => new ZWord((ushort)(a.Value % b.Value));
        
        public static ZWord operator &(ZWord a, ZWord b) => new ZWord((ushort)(a.Value & b.Value));
        public static ZWord operator |(ZWord a, ZWord b) => new ZWord((ushort)(a.Value | b.Value));
        public static ZWord operator ^(ZWord a, ZWord b) => new ZWord((ushort)(a.Value ^ b.Value));
        
        
        public static bool operator ==(ZWord left, ushort right) => left.Value == right;
        public static bool operator !=(ZWord left, ushort right) => left.Value != right;
        public static bool operator <(ZWord left, ushort right) => left.Value < right;
        public static bool operator >(ZWord left, ushort right) => left.Value > right;
        public static bool operator <=(ZWord left, ushort right) => left.Value <= right;
        public static bool operator >=(ZWord left, ushort right) => left.Value >= right;
        
        public static ZWord operator +(ZWord a, ushort b) => new ZWord((ushort)(a.Value + b));
        public static ZWord operator -(ZWord a, ushort b) => new ZWord((ushort)(a.Value - b));
        public static ZWord operator *(ZWord a, ushort b) => new ZWord((ushort)(a.Value * b));
        public static ZWord operator /(ZWord a, ushort b) => new ZWord((ushort)(a.Value / b));
        public static ZWord operator %(ZWord a, ushort b) => new ZWord((ushort)(a.Value % b));
        
        public static ZWord operator &(ZWord a, ushort b) => new ZWord((ushort)(a.Value & b));
        public static ZWord operator |(ZWord a, ushort b) => new ZWord((ushort)(a.Value | b));
        public static ZWord operator ^(ZWord a, ushort b) => new ZWord((ushort)(a.Value ^ b));
        
        public static bool operator ==(ZWord left, byte right) => left.Value == right;
        public static bool operator !=(ZWord left, byte right) => left.Value != right;
        public static bool operator <(ZWord left, byte right) => left.Value < right;
        public static bool operator >(ZWord left, byte right) => left.Value > right;
        public static bool operator <=(ZWord left, byte right) => left.Value <= right;
        public static bool operator >=(ZWord left, byte right) => left.Value >= right;
        
        public static ZWord operator +(ZWord a, byte b) => new ZWord((ushort)(a.Value + b));
        public static ZWord operator -(ZWord a, byte b) => new ZWord((ushort)(a.Value - b));
        public static ZWord operator *(ZWord a, byte b) => new ZWord((ushort)(a.Value * b));
        public static ZWord operator /(ZWord a, byte b) => new ZWord((ushort)(a.Value / b));
        public static ZWord operator %(ZWord a, byte b) => new ZWord((ushort)(a.Value % b));
        
        public static ZWord operator &(ZWord a, byte b) => new ZWord((ushort)(a.Value & b));
        public static ZWord operator |(ZWord a, byte b) => new ZWord((ushort)(a.Value | b));
        public static ZWord operator ^(ZWord a, byte b) => new ZWord((ushort)(a.Value ^ b));
        public static bool operator ==(ushort left, ZWord right) => left == right.Value;
        public static bool operator !=(ushort left, ZWord right) => left != right.Value;
        public static bool operator <(ushort left, ZWord right) => left < right.Value;
        public static bool operator >(ushort left, ZWord right) => left > right.Value;
        public static bool operator <=(ushort left, ZWord right) => left <= right.Value;
        public static bool operator >=(ushort left, ZWord right) => left >= right.Value;
        
        public static ZWord operator +(ushort a, ZWord b) => new ZWord((ushort)(a + b.Value));
        public static ZWord operator -(ushort a, ZWord b) => new ZWord((ushort)(a - b.Value));
        public static ZWord operator *(ushort a, ZWord b) => new ZWord((ushort)(a * b.Value));
        public static ZWord operator /(ushort a, ZWord b) => new ZWord((ushort)(a / b.Value));
        public static ZWord operator %(ushort a, ZWord b) => new ZWord((ushort)(a % b.Value));
        
        public static ZWord operator &(ushort a, ZWord b) => new ZWord((ushort)(a & b.Value));
        public static ZWord operator |(ushort a, ZWord b) => new ZWord((ushort)(a | b.Value));
        public static ZWord operator ^(ushort a, ZWord b) => new ZWord((ushort)(a ^ b.Value));
        
        public static bool operator ==(byte left, ZWord right) => left == right.Value;
        public static bool operator !=(byte left, ZWord right) => left != right.Value;
        public static bool operator <(byte left, ZWord right) => left < right.Value;
        public static bool operator >(byte left, ZWord right) => left > right.Value;
        public static bool operator <=(byte left, ZWord right) => left <= right.Value;
        public static bool operator >=(byte left, ZWord right) => left >= right.Value;
        
        public static ZWord operator +(byte a, ZWord b) => new ZWord((ushort)(a + b.Value));
        public static ZWord operator -(byte a, ZWord b) => new ZWord((ushort)(a - b.Value));
        public static ZWord operator *(byte a, ZWord b) => new ZWord((ushort)(a * b.Value));
        public static ZWord operator /(byte a, ZWord b) => new ZWord((ushort)(a / b.Value));
        public static ZWord operator %(byte a, ZWord b) => new ZWord((ushort)(a % b.Value));
        
        public static ZWord operator &(byte a, ZWord b) => new ZWord((ushort)(a & b.Value));
        public static ZWord operator |(byte a, ZWord b) => new ZWord((ushort)(a | b.Value));
        public static ZWord operator ^(byte a, ZWord b) => new ZWord((ushort)(a ^ b.Value));
        
        public static ZWord operator >>(ZWord a, int b) => new ZWord((ushort)(a.Value >> b));
        public static ZWord operator <<(ZWord a, int b) => new ZWord((ushort)(a.Value << b));
        public sealed class ZWordComparer : IEqualityComparer<ZWord>, IComparer<ZWord>
        {
            public static readonly ZWordComparer Instance = new ZWordComparer();
            private ZWordComparer()
            {
                
            }
            public int Compare(ZWord x, ZWord y) => x.Value.CompareTo(y.Value);
            public bool Equals(ZWord x, ZWord y) => x.Value == y.Value;
            public int GetHashCode(ZWord obj) => obj.Value.GetHashCode();
        }
    }
}