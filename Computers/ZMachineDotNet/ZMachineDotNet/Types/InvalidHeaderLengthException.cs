﻿using System;

namespace ZMachineDotNet.Types;

public class InvalidHeaderLengthException : Exception
{
    public InvalidHeaderLengthException(int byteSize)
        : base($"A Z-Program Header must be exactly 64 bytes long. The array passed is {byteSize} bytes long.")
    {
    }
    public InvalidHeaderLengthException(string message)
        : base(message)
    {
        
    }
    public InvalidHeaderLengthException(string message, Exception inner)
        : base(message, inner)
    {
        
    }
}