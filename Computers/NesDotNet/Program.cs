﻿
using System.Globalization;
using NesDotNet.Bus;
using NesDotNet.Devices;
using SixFiveOhTwo.Bus;
using SixFiveOhTwo.Bus.Devices;
using SixFiveOhTwo.Cpu;
using SixFiveOhTwo.Runner;

var memory = new MirroredMemory(0x0000, 0x07ff, 4);
var romBytes = File.ReadAllBytes(args[0]);

var romMemory = new Memory(0x4020, 0xffff, romBytes);

var bus = new NesBus();

var cpu = new Cpu(bus);

var runner = new Runner(cpu);

runner.Start();