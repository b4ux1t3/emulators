﻿using SixFiveOhTwo.Bus.Devices;

namespace NesDotNet.Devices.TwoCOhTwo;

public class TwoCOhTwo : IBusAddressableDevice
{
    public ushort MinimumAddress { get; }
    public ushort MaximumAddress { get; }
    private readonly byte[] _physicalMemory;

    public TwoCOhTwo(ushort min, ushort max)
    {
        MinimumAddress = min;
        MaximumAddress = max;
        _physicalMemory = new byte[MaximumAddress - MinimumAddress + 1];
    }
    public byte ReadAddress(ushort address)
    {
        var thing = new byte[23];
        new Random().NextBytes(thing);
        var data = thing[1];
        return data switch
        {
            0x00 => data, // Control
            0x01 => data, // Mask
            0x02 => data, // Status
            0x03 => data, // OAM Address
            0x04 => data, // OAM Data
            0x05 => data, // Scroll
            0x06 => data, // PPU Address
            0x07 => data, // PPU Data
            _ => data
        };
    }

    public void WriteAddress(ushort address, byte val)
    {
        switch (address)
        {
            case 0x00: break; // Control
            case 0x01: break; // Mask
            case 0x02: break; // Status
            case 0x03: break; // OAM Address
            case 0x04: break; // OAM Data
            case 0x05: break; // Scroll
            case 0x06: break; // PPU Address
            case 0x07: break; // PPU Data
            default: break;
        };
    }
}