﻿using Microsoft.VisualBasic.CompilerServices;
using SixFiveOhTwo.Bus.Devices;

namespace NesDotNet.Devices;

public class MirroredMemory : IBusAddressableDevice
{
    public ushort MinimumAddress { get; }
    public ushort MaximumAddress { get; }

    private int BankSize => (MaximumAddress - MinimumAddress) / _mirrors;
    private readonly int _mirrors;
    
    private readonly byte[] _physicalMemory;
    public byte ReadAddress(ushort address)
    {
        return _physicalMemory[(address - MinimumAddress) & BankSize];
    }

    public void WriteAddress(ushort address, byte val)
    {
        
        _physicalMemory[(address - MinimumAddress) & BankSize] = val;
    }

    public MirroredMemory(ushort min, ushort max, byte copies)
    {
        MinimumAddress = min;
        MaximumAddress = max;
        _mirrors = copies;
        _physicalMemory = new byte[BankSize];

    }
}