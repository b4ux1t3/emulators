﻿namespace SixFiveOhTwo;

public static class MathUtils
{
    public static bool IsNegative(this byte self) => (self & 0x0080) == 0x0080;

    public static string ToHexString(this byte self)
    {
        var str = $"{self:x8}".TrimStart('0').PadLeft(2, '0');
        return $"0x{str}";
    }
    
    public static string ToHexString(this ushort self)
    {
        var str = $"{self:x8}".TrimStart('0').PadLeft(4, '0');
        return $"0x{str}";
    }
}