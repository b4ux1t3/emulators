using System.Globalization;

namespace SixFiveOhTwo.Runner;

using SixFiveOhTwo.Cpu;

public class Runner
{
    private readonly Cpu _cpu;
    private bool _running = true;
    public Runner(Cpu cpu)
    {
        _cpu = cpu;
    }

    public void Start()
    {
        _cpu.Reset();
        while (_running)
        {
            Prompt();
            CheckKey();
        }
    }

    private void Prompt()
    {
        Console.Clear();
        Console.WriteLine(_cpu);
        Console.WriteLine("(n) - jump to next instruction | (space) - cycle clock | (q) - quit");
    }

    private void CheckKey()
    {
        var key = Console.ReadKey(true);
        switch (key.Key)
        {
            case ConsoleKey.N:
                _cpu.Clock(true);
                break;
            case ConsoleKey.Spacebar:
                _cpu.Clock();
                break;
            case ConsoleKey.Q:
                _running = false;
                break;
            case ConsoleKey.D:
                SetPc();
                break;
        }
    }

    private void SetPc()
    {
        Console.Write("\nEnter New ProgramCounter:\t0x");
        var input = Console.ReadLine();
        var success = ushort.TryParse(input, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out var number);
        if (success)
            _cpu.SetProgramCounter(number);
        else
            Console.WriteLine("Invalid entry. Must be a hex number be lower than 0x10000");
    }
}