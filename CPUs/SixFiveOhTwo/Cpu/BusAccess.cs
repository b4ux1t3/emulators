﻿using SixFiveOhTwo.Bus;

namespace SixFiveOhTwo.Cpu;

public partial class Cpu
{
    private readonly IBus _bus;

    private byte Read(ushort address) => _bus.Read(address);
    private void Write(ushort address, byte value) => _bus.Write(address, value);
}