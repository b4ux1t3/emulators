﻿using System.Text;

namespace SixFiveOhTwo.Cpu;

/// <summary>
///  Contains debugging tools, like the ToString implementation
/// </summary>
public partial class Cpu
{
    private readonly StringBuilder _sb = new();
    private const string StatusHeader = "C|Z|I|D|B|U|V|N";

    public override string ToString()
    {
        _sb.Clear();


        _sb.Append($"Acc:\t{Accumulator.ToHexString()} ({Accumulator})");
        _sb.Append('\t');
        _sb.Append(StatusHeader);
        _sb.Append('\n');
        _sb.Append($"Idx:\t{Idx.ToHexString()} ({Idx})");
        _sb.Append('\t');
        _sb.Append("---------------\n");
        _sb.Append($"Idy:\t{Idy.ToHexString()} ({Idy})");
        _sb.Append('\t');
        _sb.Append(C ? '1' : '0');
        _sb.Append('|');
        _sb.Append(Z ? '1' : '0');
        _sb.Append('|');
        _sb.Append(I ? '1' : '0');
        _sb.Append('|');
        _sb.Append(D ? '1' : '0');
        _sb.Append('|');
        _sb.Append(B ? '1' : '0');
        _sb.Append('|');
        _sb.Append(U ? '1' : '0');
        _sb.Append('|');
        _sb.Append(V ? '1' : '0');
        _sb.Append('|');
        _sb.Append(N ? '1' : '0');
        _sb.Append('\n');
        
        _sb.Append('\n');
        _sb.Append($"PC:\t{ProgramCounter.ToHexString()} ({ProgramCounter})\t");
        _sb.Append($"Stk:\t{StackPointer.ToHexString()} ({StackPointer})\n");
        _sb.Append('\n');
        _sb.Append("Last Instruction\tNext Instruction\n");
        _sb.Append($"{_opcode.ToHexString()}: {CurrentInstruction}");
        _sb.Append("\t\t");
        var nextOpcode = Read(ProgramCounter);
        _sb.Append($"{nextOpcode.ToHexString()}: {InstructionLookup[nextOpcode]}\n\n");
        _sb.Append($"Clock: {_clockCount} | Next Instruction Cycles: {_cycles}\n");
        return _sb.ToString();
    } 
}