namespace SixFiveOhTwo.Cpu;

/// <summary>
/// Emulation of the hardware, including interrupts, reset signal, and fetching of data.
/// Nothing here is specific to the 6502, it just provides convenient abstractions for
/// some of the actions and data that you need when dealing with emulation.
/// </summary>
public partial class Cpu
{
    // Emulation Helpers, courtesy of Javidx9
    private byte   _fetched;    // Represents the working input value to the ALU
    private int    _temp;       // A convenience variable used everywhere
    private ushort _addrAbs;    // All used memory addresses end up in here
    private ushort _addrRel;    // Represents absolute address following a branch
    private byte   _opcode;     // Is the instruction byte
    private byte   _cycles;	    // Counts how many cycles the instruction has remaining
    private uint   _clockCount; // A global accumulation of the number of clocks
    
    // A shortcut for getting the next address on the stack.
    private ushort StackAddress => (ushort) (StackBegin + StackPointer);
    private Instruction CurrentInstruction => InstructionLookup[_opcode];

    public void Reset()
    {
        StackPointer = 0xfd;
        Accumulator = 0;
        Idx = 0;
        Idy = 0;
        StatusFlags = 0x00 | UnusedFlag;
        
        _addrAbs = 0xfffc;

        var lo = Read(_addrAbs);
        var hi = Read((ushort) (_addrAbs + 1));

        ProgramCounter = (ushort) ((hi << 8) | lo);
        
        _fetched = 0x00;  
        _temp = 0x00000000;
        _addrAbs = 0x0000;
        _addrRel = 0x00;  
        _opcode = 0x00;  
        _cycles = 8;	    
        _clockCount = 0;
    }

    public void Clock(bool nextInstruction = false)
    {
        if (nextInstruction)
        {
            _clockCount += _cycles;
            _cycles = 0;
            
        }
        
        if (_cycles == 0)
        {
            _opcode = Read(ProgramCounter++);
            
            _cycles = CurrentInstruction.Cycles;

            var cycleCheckAdd = CurrentInstruction.AddressingMode();
            var cycleCheckOp = CurrentInstruction.Handler();

            _cycles += (byte) (cycleCheckOp & cycleCheckAdd);
        }
        
        _cycles--;
        _clockCount++;
    }
    
    /// <summary>
    /// Can be ignored, depending on the interrupt enable flag (<see cref="InterruptDisableFlag"/>).
    /// </summary>
    public void IRQ()
    {
        if (I) return;
        
        Write(StackAddress, (byte)((ProgramCounter >> 8) & 0x00ff));
        StackPointer--;
        Write(StackAddress, (byte)(ProgramCounter & 0x00ff));
        StackPointer--;

        B = false;
        U = true;
        I = true;

        Write(StackAddress, StatusFlags);
        StackPointer--;

        _addrAbs = 0xfffe;
        var lo = Read(_addrAbs);
        var hi = Read((ushort) (_addrAbs + 1));
        ProgramCounter = (ushort) ((hi << 8) | lo);

        _cycles = 7;
    }

    /// <summary>
    /// Can never be disabled.
    /// </summary>
    public void NMI()
    {
        Write(StackAddress, (byte)((ProgramCounter >> 8) & 0x00ff));
        StackPointer--;
        Write(StackAddress, (byte)(ProgramCounter & 0x00ff));
        StackPointer--;

        B = false;
        U = true;
        I = true;

        Write(StackAddress, StatusFlags);
        StackPointer--;

        _addrAbs = 0xfffa;
        var lo = Read(_addrAbs);
        var hi = Read((ushort) (_addrAbs + 1));
        ProgramCounter = (ushort) ((hi << 8) | lo);

        _cycles = 8;
    }
    
    private byte Fetch()
    {
        if (InstructionLookup[_opcode].AddressingMode != IMP)
        {
            _fetched = Read(_addrAbs);
        }

        return _fetched;
    }

    public void SetProgramCounter(ushort address)
    {
        ProgramCounter = address;
    }
}