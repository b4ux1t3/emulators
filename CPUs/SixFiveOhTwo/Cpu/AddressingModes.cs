namespace SixFiveOhTwo.Cpu;

/// <summary>
///  The implementations for all of the 6502 addressing modes.
/// </summary>
public partial class Cpu
{
    private delegate byte AddressingModeHandler();
    private byte IMP()
    {
        _fetched = Accumulator;
        return 0;
    }

    private byte IMM()
    {
        // TODO: Verify this is right. We need to get the byte _after_ the current byte,
        // but we should read it later from the PC
        _addrAbs = ProgramCounter++;
        return 0;
    }

    private byte ZP0()
    {
        _addrAbs = Read(ProgramCounter);
        ProgramCounter++;
        _addrAbs &= 0x00ff;
        return 0;
    }

    private byte ZPX()
    {
        _addrAbs = (ushort) (Read(ProgramCounter) + Idx);
        ProgramCounter++;
        _addrAbs &= 0x00ff;
        return 0;
    }

    private byte ZPY()
    {
        _addrAbs = (ushort) (Read(ProgramCounter) + Idy);
        ProgramCounter++;
        _addrAbs &= 0x00ff;
        return 0;
    }

    private byte REL()
    {
        _addrRel = Read(ProgramCounter++);

        if ((_addrRel & 0x80) == 0x80)
            _addrRel |= 0xff00;
        
        return 0;
    }

    private byte ABS()
    {
        ushort lo = Read(ProgramCounter++);
        ushort hi = Read(ProgramCounter++);

        _addrAbs = (ushort) ((hi << 8) | lo);
        
        return 0;
    }

    private byte ABX()
    {
        ushort lo = Read(ProgramCounter++);
        ushort hi = Read(ProgramCounter++);

        _addrAbs = (ushort) ((hi << 8) | lo);
        _addrAbs += Idx;
        
        return (byte) ((_addrAbs & 0xff00) != (hi << 8) ? 1 : 0);
    }

    private byte ABY()
    {
        ushort lo = Read(ProgramCounter++);
        ushort hi = Read(ProgramCounter++);

        _addrAbs = (ushort) ((hi << 8) | lo);
        _addrAbs += Idy;
        
        return (byte) ((_addrAbs & 0xff00) != hi << 8 ? 1 : 0);
    }

    private byte IND()
    {
        ushort lo = Read(ProgramCounter++);
        ushort hi = Read(ProgramCounter++);

        var ptr = (ushort) ((hi << 8) | lo);
        
        // There's a hardware bug here that needs to be accounted for.
        if (lo == 0x00ff)
        {
            _addrAbs = (ushort) (Read((ushort) (ptr & 0xff00)) << 8 | Read((ushort) (ptr + 0)));

        }
        else
        {
            _addrAbs = (ushort) (Read((ushort) (ptr + 1)) << 8 | Read((ushort) (ptr + 0)));
        }

        return 0;
    }

    private byte IZX()
    {
        ushort t = Read(ProgramCounter++);

        ushort lo = Read((ushort) ((t + Idx) & 0x00ff));
        ushort hi = Read((ushort) ((t + Idx + 1) & 0x00ff));
        
        _addrAbs = (ushort) ((hi << 8) | lo);
        
        return 0;
    }

    private byte IZY()
    {
        ushort t = Read(ProgramCounter++);

        ushort lo = Read((ushort) (t & 0x00ff));
        ushort hi = Read((ushort) ((t+ 1) & 0x00ff));
        
        _addrAbs = (ushort) ((hi << 8) | lo);
        _addrAbs += Idy;
        
        return (byte) ((_addrAbs & 0xff00) != hi << 8 ? 1 : 0);
    }
}