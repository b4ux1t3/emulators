﻿using SixFiveOhTwo.Bus;

namespace SixFiveOhTwo.Cpu;

/// <summary>
/// This class is split across many files. Each file contains properties and methods relevant to a specific
/// area of CPU operation. There may, at one point, be a table of contents here. If not, just use Intellisense, dangit.
/// </summary>
public partial class Cpu
{
    private IReadOnlyList<Instruction> InstructionLookup;
    private const ushort StackBegin = 0x0100;
    private const ushort ProgramSourcedInterruptAddress = 0xfffe;
    private const ushort NegativeBit = 0x0080;

    public Cpu(IBus bus)
    {
        _bus = bus;
        // Yeah, I know. Deal with it.
        InstructionLookup = new Instruction[]
        {
            new ( "BRK", BRK, IMM, 7 ), new ( "ORA", ORA, IZX, 6 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 3 ), new ( "ORA", ORA, ZP0, 3 ), new ( "ASL", ASL, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "PHP", PHP, IMP, 3 ), new ( "ORA", ORA, IMM, 2 ), new ( "ASL", ASL, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "???", NOP, IMP, 4 ), new ( "ORA", ORA, ABS, 4 ), new ( "ASL", ASL, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BPL", BPL, REL, 2 ), new ( "ORA", ORA, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "ORA", ORA, ZPX, 4 ), new ( "ASL", ASL, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "CLC", CLC, IMP, 2 ), new ( "ORA", ORA, ABY, 4 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "ORA", ORA, ABX, 4 ), new ( "ASL", ASL, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
            new ( "JSR", JSR, ABS, 6 ), new ( "AND", AND, IZX, 6 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "BIT", BIT, ZP0, 3 ), new ( "AND", AND, ZP0, 3 ), new ( "ROL", ROL, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "PLP", PLP, IMP, 4 ), new ( "AND", AND, IMM, 2 ), new ( "ROL", ROL, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "BIT", BIT, ABS, 4 ), new ( "AND", AND, ABS, 4 ), new ( "ROL", ROL, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BMI", BMI, REL, 2 ), new ( "AND", AND, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "AND", AND, ZPX, 4 ), new ( "ROL", ROL, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "SEC", SEC, IMP, 2 ), new ( "AND", AND, ABY, 4 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "AND", AND, ABX, 4 ), new ( "ROL", ROL, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
            new ( "RTI", RTI, IMP, 6 ), new ( "EOR", EOR, IZX, 6 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 3 ), new ( "EOR", EOR, ZP0, 3 ), new ( "LSR", LSR, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "PHA", PHA, IMP, 3 ), new ( "EOR", EOR, IMM, 2 ), new ( "LSR", LSR, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "JMP", JMP, ABS, 3 ), new ( "EOR", EOR, ABS, 4 ), new ( "LSR", LSR, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BVC", BVC, REL, 2 ), new ( "EOR", EOR, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "EOR", EOR, ZPX, 4 ), new ( "LSR", LSR, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "CLI", CLI, IMP, 2 ), new ( "EOR", EOR, ABY, 4 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "EOR", EOR, ABX, 4 ), new ( "LSR", LSR, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
            new ( "RTS", RTS, IMP, 6 ), new ( "ADC", ADC, IZX, 6 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 3 ), new ( "ADC", ADC, ZP0, 3 ), new ( "ROR", ROR, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "PLA", PLA, IMP, 4 ), new ( "ADC", ADC, IMM, 2 ), new ( "ROR", ROR, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "JMP", JMP, IND, 5 ), new ( "ADC", ADC, ABS, 4 ), new ( "ROR", ROR, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BVS", BVS, REL, 2 ), new ( "ADC", ADC, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "ADC", ADC, ZPX, 4 ), new ( "ROR", ROR, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "SEI", SEI, IMP, 2 ), new ( "ADC", ADC, ABY, 4 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "ADC", ADC, ABX, 4 ), new ( "ROR", ROR, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
            new ( "???", NOP, IMP, 2 ), new ( "STA", STA, IZX, 6 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 6 ), new ( "STY", STY, ZP0, 3 ), new ( "STA", STA, ZP0, 3 ), new ( "STX", STX, ZP0, 3 ), new ( "???", XXX, IMP, 3 ), new ( "DEY", DEY, IMP, 2 ), new ( "???", NOP, IMP, 2 ), new ( "TXA", TXA, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "STY", STY, ABS, 4 ), new ( "STA", STA, ABS, 4 ), new ( "STX", STX, ABS, 4 ), new ( "???", XXX, IMP, 4 ),
            new ( "BCC", BCC, REL, 2 ), new ( "STA", STA, IZY, 6 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 6 ), new ( "STY", STY, ZPX, 4 ), new ( "STA", STA, ZPX, 4 ), new ( "STX", STX, ZPY, 4 ), new ( "???", XXX, IMP, 4 ), new ( "TYA", TYA, IMP, 2 ), new ( "STA", STA, ABY, 5 ), new ( "TXS", TXS, IMP, 2 ), new ( "???", XXX, IMP, 5 ), new ( "???", NOP, IMP, 5 ), new ( "STA", STA, ABX, 5 ), new ( "???", XXX, IMP, 5 ), new ( "???", XXX, IMP, 5 ),
            new ( "LDY", LDY, IMM, 2 ), new ( "LDA", LDA, IZX, 6 ), new ( "LDX", LDX, IMM, 2 ), new ( "???", XXX, IMP, 6 ), new ( "LDY", LDY, ZP0, 3 ), new ( "LDA", LDA, ZP0, 3 ), new ( "LDX", LDX, ZP0, 3 ), new ( "???", XXX, IMP, 3 ), new ( "TAY", TAY, IMP, 2 ), new ( "LDA", LDA, IMM, 2 ), new ( "TAX", TAX, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "LDY", LDY, ABS, 4 ), new ( "LDA", LDA, ABS, 4 ), new ( "LDX", LDX, ABS, 4 ), new ( "???", XXX, IMP, 4 ),
            new ( "BCS", BCS, REL, 2 ), new ( "LDA", LDA, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 5 ), new ( "LDY", LDY, ZPX, 4 ), new ( "LDA", LDA, ZPX, 4 ), new ( "LDX", LDX, ZPY, 4 ), new ( "???", XXX, IMP, 4 ), new ( "CLV", CLV, IMP, 2 ), new ( "LDA", LDA, ABY, 4 ), new ( "TSX", TSX, IMP, 2 ), new ( "???", XXX, IMP, 4 ), new ( "LDY", LDY, ABX, 4 ), new ( "LDA", LDA, ABX, 4 ), new ( "LDX", LDX, ABY, 4 ), new ( "???", XXX, IMP, 4 ),
            new ( "CPY", CPY, IMM, 2 ), new ( "CMP", CMP, IZX, 6 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "CPY", CPY, ZP0, 3 ), new ( "CMP", CMP, ZP0, 3 ), new ( "DEC", DEC, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "INY", INY, IMP, 2 ), new ( "CMP", CMP, IMM, 2 ), new ( "DEX", DEX, IMP, 2 ), new ( "???", XXX, IMP, 2 ), new ( "CPY", CPY, ABS, 4 ), new ( "CMP", CMP, ABS, 4 ), new ( "DEC", DEC, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BNE", BNE, REL, 2 ), new ( "CMP", CMP, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "CMP", CMP, ZPX, 4 ), new ( "DEC", DEC, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "CLD", CLD, IMP, 2 ), new ( "CMP", CMP, ABY, 4 ), new ( "NOP", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "CMP", CMP, ABX, 4 ), new ( "DEC", DEC, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
            new ( "CPX", CPX, IMM, 2 ), new ( "SBC", SBC, IZX, 6 ), new ( "???", NOP, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "CPX", CPX, ZP0, 3 ), new ( "SBC", SBC, ZP0, 3 ), new ( "INC", INC, ZP0, 5 ), new ( "???", XXX, IMP, 5 ), new ( "INX", INX, IMP, 2 ), new ( "SBC", SBC, IMM, 2 ), new ( "NOP", NOP, IMP, 2 ), new ( "???", SBC, IMP, 2 ), new ( "CPX", CPX, ABS, 4 ), new ( "SBC", SBC, ABS, 4 ), new ( "INC", INC, ABS, 6 ), new ( "???", XXX, IMP, 6 ),
            new ( "BEQ", BEQ, REL, 2 ), new ( "SBC", SBC, IZY, 5 ), new ( "???", XXX, IMP, 2 ), new ( "???", XXX, IMP, 8 ), new ( "???", NOP, IMP, 4 ), new ( "SBC", SBC, ZPX, 4 ), new ( "INC", INC, ZPX, 6 ), new ( "???", XXX, IMP, 6 ), new ( "SED", SED, IMP, 2 ), new ( "SBC", SBC, ABY, 4 ), new ( "NOP", NOP, IMP, 2 ), new ( "???", XXX, IMP, 7 ), new ( "???", NOP, IMP, 4 ), new ( "SBC", SBC, ABX, 4 ), new ( "INC", INC, ABX, 7 ), new ( "???", XXX, IMP, 7 ),
        };

        Reset();
    }

    private record Instruction(string Name, OpCodeHandler Handler, AddressingModeHandler AddressingMode, byte Cycles)
    {
        public override string ToString()
        {
            return $"{AddressingMode.Method.Name}->{Name}";
        }
    }
}