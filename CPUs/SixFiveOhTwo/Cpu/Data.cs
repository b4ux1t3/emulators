﻿namespace SixFiveOhTwo.Cpu;

/// <summary>
/// Contains any data that actually exists within a 6502.
/// </summary>
public partial class Cpu
{
    private const byte CarryFlag = 0b0000_0001;
    private const byte ZeroFlag = 0b0000_0010;
    private const byte InterruptDisableFlag = 0b0000_0100;
    private const byte DecimalModeFlag = 0b0000_1000;
    private const byte BreakCommandFlag = 0b0001_0000;
    private const byte UnusedFlag = 0b0010_0000;
    private const byte OverflowFlag = 0b0100_0000;
    private const byte NegativeFlag = 0b1000_0000;
    
    // Physical Registers
    public ushort ProgramCounter { get; private set; }
    public byte StackPointer { get; private set; }
    public byte Accumulator { get; private set; }
    public byte Idx { get; private set; }
    public byte Idy { get; private set; }
    public byte StatusFlags { get; private set; }

    public bool C {
        get => (StatusFlags & CarryFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | CarryFlag)
                : (byte) (StatusFlags & ~CarryFlag);
    }
    public bool Z {
        get => (StatusFlags & ZeroFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | ZeroFlag)
                : (byte) (StatusFlags & ~ZeroFlag);
    }
    public bool I {
        get => (StatusFlags & InterruptDisableFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | InterruptDisableFlag)
                : (byte) (StatusFlags & ~InterruptDisableFlag);
    }
    public bool D {
        get => (StatusFlags & DecimalModeFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | DecimalModeFlag)
                : (byte) (StatusFlags & ~DecimalModeFlag);
    }
    public bool B {
        get => (StatusFlags & BreakCommandFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | BreakCommandFlag)
                : (byte) (StatusFlags & ~BreakCommandFlag);
    }
    public bool U
    {
        get => (StatusFlags & UnusedFlag) > 0;
        private set =>
            StatusFlags = value
                ? (byte) (StatusFlags | UnusedFlag)
                : (byte) (StatusFlags & ~UnusedFlag);
    }
    public bool V {
        get => (StatusFlags & OverflowFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | OverflowFlag)
                : (byte) (StatusFlags & ~OverflowFlag);
    }
    public bool N {
        get => (StatusFlags & NegativeFlag) > 0;
        private set =>
            StatusFlags = value 
                ? (byte) (StatusFlags | NegativeFlag)
                : (byte) (StatusFlags & ~NegativeFlag);
    }
}