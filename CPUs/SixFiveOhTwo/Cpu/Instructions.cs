namespace SixFiveOhTwo.Cpu;

/// <summary>
/// The implementations for all of the 6502 opcodes.
/// </summary>
public partial class Cpu
{
    private delegate byte OpCodeHandler();

    
    private byte ADC()
    {
        Fetch();

        _temp = Accumulator + _fetched;
        if (C) _temp++;

        C = _temp > 255;
        Z = (_temp & 0x00ff) == 0;
        V = (~(Accumulator ^ _fetched) & (Accumulator ^ _temp) & NegativeBit) == NegativeBit;
        N = (_temp & 0x80) == 0x80;
        Accumulator = (byte) (_temp & 0x00ff);
        
        return 1;
    }

    private byte AND()
    {
        Fetch();
        Accumulator = (byte) (Accumulator & _fetched);
        Z = Accumulator == 0x00;

        return 1;
    }

    private byte ASL()
    {
        Fetch();
        _temp = _fetched << 1;
        C = _fetched.IsNegative();
        N = (_temp & NegativeBit) == NegativeBit;
        Z = _temp == 0;
        if (CurrentInstruction.AddressingMode == IMP)
            Accumulator = (byte) _temp;
        else
            Write(_addrAbs, (byte) _temp);

        return 0;
    }

    private byte BCC()
    {
        if (C) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;    
    }

    private byte BCS()
    {
        if (!C) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;
    }

    private byte BEQ()
    {
        if (!Z) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;    }

    private byte BIT()
    {
        Fetch();
        _temp = Accumulator & _fetched;
        Z = _temp == 0;
        V = (_fetched & 0x0040) == 0x0040;
        N = (_fetched & NegativeBit) == NegativeBit;

        return 0;
    }

    private byte BMI()
    {
        if (!N) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;    
    }

    private byte BNE()
    {
        if (Z) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;
    }

    private byte BPL()
    {
        if (N) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;    
    }

    private byte BRK()
    {
        ProgramCounter++;

        I = true;
        
        Write(StackAddress, (byte) ((ProgramCounter >> 8) & 0x00ff));
        StackPointer++;
        Write(StackAddress, (byte) (ProgramCounter & 0x00ff));
        StackPointer++;

        B = true;
        Write(StackAddress, StatusFlags);
        StackPointer--;
        B = false;

        ProgramCounter = (ushort) (Read(ProgramSourcedInterruptAddress) | (Read(ProgramSourcedInterruptAddress + 1) << 8));
        return 0;
    }

    private byte BVC()
    {
        if (!V) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;
    }

    private byte BVS()
    {
        if (V) return 0;
        
        _cycles++;
        _addrAbs = (ushort) (ProgramCounter + _addrRel);

        // If we cross a page boundary, add another cycle.
        if ((_addrAbs & 0xff00) != (ProgramCounter & 0xff00)) _cycles++;

        ProgramCounter = _addrAbs;

        return 0;    
    }

    private byte CLC()
    {
        C = false;
        return 0;
    }

    private byte CLD()
    {
        D = false;
        return 0;
    }

    private byte CLI()
    {
        I = false;
        return 0;
    }

    private byte CLV()
    {
        V = false;
        return 0;
    }

    private byte CMP()
    {
        Fetch();
        _temp = Accumulator - _fetched;
        C = Accumulator >= _fetched;
        Z = (_temp & 0x00ff) == 0x0000;
        N = (_temp & NegativeBit) == NegativeBit;
        
        return 1;
    }

    private byte CPX()
    {
        Fetch();
        _temp = Idx - _fetched;
        C = Idx >= _fetched;
        Z = (_temp & 0x00ff) == 0x0000;
        N = (_temp & NegativeBit) == NegativeBit;
        
        return 1;    
    }

    private byte CPY()
    {
        Fetch();
        _temp = Idy - _fetched;
        C = Idy >= _fetched;
        Z = (_temp & 0x00ff) == 0x0000;
        N = (_temp & NegativeBit) == NegativeBit;
        
        return 1;
    }

    private byte DEC()
    {
        Fetch();
        _temp = _fetched - 1;
        Write(_addrAbs, (byte) _temp);
        Z = (byte) _temp == 0;
        N = (_temp & NegativeBit) == NegativeBit;
        return 0;
    }

    private byte DEX()
    {
        Idx--;
        Z = Idx == 0;
        N = Idx.IsNegative();

        return 0;
    }

    private byte DEY()
    {
        Idy--;
        Z = Idy == 0;
        N = Idy.IsNegative();

        return 0;
    }

    private byte EOR()
    {
        Fetch();
        Accumulator = (byte) (Accumulator ^ _fetched);
        Z = Accumulator == 0;
        N = Accumulator.IsNegative();
        
        return 1;
    }

    private byte INC()
    {
        Fetch();
        _temp = _fetched + 1;
        Write(_addrAbs, (byte) _temp);
        Z = (byte) _temp == 0;
        N = (_temp & NegativeBit) == NegativeBit;
        return 0;
    }

    private byte INX()
    {
        Idx++;
        Z = Idx == 0;
        N = Idx.IsNegative();

        return 0;
    }

    private byte INY()
    {
        Idy++;
        Z = Idy == 0;
        N = Idy.IsNegative();

        return 0;
    }

    private byte JMP()
    {
        ProgramCounter = _addrAbs;

        return 0;
    }

    private byte JSR()
    {
        ProgramCounter--;
        
        Write(StackAddress, (byte) ((ProgramCounter >> 8) & 0x00ff));
        StackPointer--;
        Write(StackAddress, (byte) (ProgramCounter & 0x00ff));
        StackPointer--;
        
        ProgramCounter = _addrAbs;

        return 0;

    }

    private byte LDA()
    {
        Fetch();
        Accumulator = _fetched;

        Z = Accumulator == 0;
        N = Accumulator.IsNegative();

        return 1;
    }

    private byte LDX()
    {
        Fetch();
        Idx = _fetched;

        Z = Idx == 0;
        N = Idx.IsNegative();

        return 1;
    }

    private byte LDY()
    {
        Fetch();
        Idy = _fetched;

        Z = Idy == 0;
        N = Idy.IsNegative();

        return 1;
    }

    private byte LSR()
    {
        Fetch();
        _temp = _fetched >> 1;
        C = (_fetched & 1) == 1;
        N = ((byte) _temp).IsNegative();
        Z = _temp == 0;
        
        if (CurrentInstruction.AddressingMode == IMP)
            Accumulator = (byte) _temp;
        else
            Write(_addrAbs, (byte) _temp);

        return 0;
    }

    private byte NOP()
    {
        switch (_opcode)
        {
            case 0x1C:
            case 0x3C:
            case 0x5C:
            case 0x7C:
            case 0xDC:
            case 0xFC:
                return 1;
        }
        return 0;
    }

    private byte ORA()
    {
        Fetch();
        Accumulator |= (byte) (Accumulator | _fetched);
        Z = Accumulator == 0;
        N = Accumulator.IsNegative();

        return 1;
    }

    private byte PHA()
    {
        Write(StackAddress, Accumulator);
        StackPointer--;
        return 0;
    }

    private byte PHP()
    {
        Write(StackAddress, (byte)(StatusFlags | BreakCommandFlag | UnusedFlag));
        B = false;
        U = false;
        StackPointer--;

        return 0;
    }

    private byte PLA()
    {
        StackPointer++;
        Accumulator = Read(StackAddress);
        Z = Accumulator == 0;
        N = Accumulator.IsNegative();
        
        return 0;
    }

    private byte PLP()
    {
        StackPointer++;
        StatusFlags = Read(StackAddress);
        
        return 0;
    }

    private byte ROL()
    {
        Fetch();
        _temp = _fetched << 1;
        _temp |= _fetched & 1;
        
        if (CurrentInstruction.AddressingMode == IMP)
            Accumulator = (byte) _temp;
        else
            Write(_addrAbs, (byte) _temp);
        
        Z = (byte) _temp == 0;
        C = _fetched.IsNegative();
        N = ((byte) _temp).IsNegative();
        
        return 0;
    }

    private byte ROR()
    {
        Fetch();
        _temp = _fetched >> 1;
        _temp |= _fetched & 0b1000_0000;
        
        if (CurrentInstruction.AddressingMode == IMP)
            Accumulator = (byte) _temp;
        else
            Write(_addrAbs, (byte) _temp);
        
        Z = (byte) _temp == 0;
        C = _fetched.IsNegative();
        N = ((byte) _temp).IsNegative();
        
        return 0;
    }

    private byte RTI()
    {
        StackPointer++;
        StatusFlags = Read(StackAddress);
        
        StatusFlags &= ~BreakCommandFlag & 0x00ff;
        StatusFlags &= ~UnusedFlag & 0x00ff;

        StackPointer++;
        ProgramCounter = Read(StackAddress);
        StackPointer++;
        ProgramCounter |= (ushort) (Read(StackAddress) << 8);
        
        return 0;
    }

    private byte RTS()
    {
        StackPointer++;
        ProgramCounter = Read(StackAddress);
        StackPointer++;
        ProgramCounter |= (ushort)(Read(StackAddress) << 8);
	
        ProgramCounter++;
        return 0;    }

    private byte SBC()
    {
        Fetch();

        var value = _fetched ^ 0x00ff;
        _temp = Accumulator + value;
        if (C) _temp++;

        C = _temp > 255;
        Z = (_temp & 0x00ff) == 0;
        V = (~(Accumulator ^ _temp) & (value ^ _temp) & NegativeBit) == NegativeBit;
        N = (_temp & 0x80) == 0x80;
        Accumulator = (byte) (_temp & 0x00ff);
        
        return 1;
    }

    private byte SEC()
    {
        C = true;
        return 0;
    }

    private byte SED()
    {
        D = true;
        return 0;
    }

    private byte SEI()
    {
        I = true;
        return 0;
    }

    private byte STA()
    {
        Write(_addrAbs, Accumulator);
        return 0;
    }

    private byte STX()
    {
        Write(_addrAbs, Idx);
        return 0;
    }

    private byte STY()
    {
        Write(_addrAbs, Idy);
        return 0;
    }

    private byte TAX()
    {
        Idx = Accumulator;
        Z = Idx == 0;
        N = Idx.IsNegative();
        return 0;
    }

    private byte TAY()
    {
        Idy = Accumulator;
        Z = Idy == 0;
        N = Idy.IsNegative();
        return 0;
        
    }

    private byte TSX()
    {
        Idx = StackPointer;
        
        Z = Idx == 0;
        N = Idx.IsNegative();
        return 0;
    }

    private byte TXA()
    {
        Accumulator = Idx;
        
        Z = Accumulator == 0;
        N = Accumulator.IsNegative();
        return 0;
    }

    private byte TXS()
    {
        StackPointer = Idx;
        
        Z = StackPointer == 0;
        N = StackPointer.IsNegative();
        return 0;
    }

    private byte TYA()
    {
        Accumulator = Idy;
        
        Z = Accumulator == 0;
        N = Accumulator.IsNegative();
        return 0;
    }

    private byte XXX()
    {
        return 0;
    }
}
