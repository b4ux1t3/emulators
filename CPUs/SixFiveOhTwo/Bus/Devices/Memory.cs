namespace SixFiveOhTwo.Bus.Devices;

public class Memory : IBusAddressableDevice {
    private byte[] PhysicalMemory { get; }
    public ushort MinimumAddress { get; } 
    public ushort MaximumAddress { get; }

    public Memory(ushort min, ushort max)
    {
        if (max < min) throw new InvalidOperationException($"The maximum value ({max:x8}) must be larger than the minimum value ({min:x8})");
        MinimumAddress = min;
        MaximumAddress = max;
        PhysicalMemory =  new byte[max - min + 1];
        Initialize();
    }
    public Memory(ushort min, ushort max, byte[] contents): this(min, max)
    {
        contents.CopyTo(PhysicalMemory, 0);
    }
    public void Initialize()
    {
        for (var i = 0; i < PhysicalMemory.Length; i++)
        {
            PhysicalMemory[i] = 0;
        }
    }

    public byte ReadAddress(ushort address) => PhysicalMemory[address - MinimumAddress];
    public void WriteAddress(ushort address, byte val) => PhysicalMemory[address - MinimumAddress] = val;

}