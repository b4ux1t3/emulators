﻿namespace SixFiveOhTwo.Bus.Devices;

public interface IBusAddressableDevice
{
    public ushort MinimumAddress { get; }
    public ushort MaximumAddress { get; }
    public byte ReadAddress(ushort address);
    public void WriteAddress(ushort address, byte val);
}