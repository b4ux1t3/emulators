using SixFiveOhTwo.Bus.Devices;

namespace SixFiveOhTwo.Bus;

/// <summary>
/// This is an example bus implementation.
/// </summary>
public class SystemBus : IBus
{
    
    private readonly IReadOnlyList<IBusAddressableDevice> _devices;

    public SystemBus()
    {
        // Realistically, we're going to be defining devices from a config file or something, but, for now,
        // we'll initialize a memory object that fills the addressable range.
        _devices = new[]
        {
            new Memory(0x0000, 0xffff)
        }; 
    }
    public SystemBus(IReadOnlyList<IBusAddressableDevice> devices)
    {
        if (devices.Count == 0)
            throw new InvalidOperationException("List of devices cannot be empty.");
        // Eventually, we'll do some checking to nmake sure devices don't overlap their memory areas, but we
        // don't need to do that right now.
        _devices = devices;
    }

    public SystemBus(IBusAddressableDevice device)
    {
        _devices = new[] {device};
    }

    // We _might_ end up doing what a real bus does and pass every request to every device and let them decide 
    // whether or not to respond. That might incur some overhead we don't need, though.
    public byte Read(ushort address)
    {
        var deviceMatch = 
            _devices.Where(device => device.MinimumAddress <= address && device.MaximumAddress >= address);
        var busAddressableDevices = deviceMatch as IBusAddressableDevice[] ?? deviceMatch.ToArray();
        var matches = busAddressableDevices.Length;
        return matches switch
        {
            > 1 => throw new InvalidOperationException(
                $"Too many devices listening to one address range! Address: {address:x8}"),
            0 => 0x00,
            _ => busAddressableDevices.First().ReadAddress(address)
        };
    }
    public void Write(ushort address, byte val)
    {
        var deviceMatch = 
            _devices.Where(device => device.MinimumAddress <= address && device.MaximumAddress >= address);
        var busAddressableDevices = deviceMatch as IBusAddressableDevice[] ?? deviceMatch.ToArray();
        var matches = busAddressableDevices.Length;
        if (matches == 1)
            busAddressableDevices.First().WriteAddress(address, val);
        
    } 

}